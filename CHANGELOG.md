# Changelog


#### CassandraTools-1.3 - 11/09/19:
    Add an attriute list from cassandra according to a wildcard

#### CassandraTools-1.2 - 17/06/19:
    Add a "Open from Database"iteme menu
    Check if attributes already exist in HDB before adding

#### CassandraTools-1.1 - 14/06/19:
    Get attribute list from HDB table.

#### CassandraTools-1.0 - 13/03/19:
    Initial Revision
