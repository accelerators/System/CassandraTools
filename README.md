# Project CassandraTools

Maven Java project

This application is able to add attribute(s) to HDB++
 with another name (new CS or domain)


## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/System/CassandraTools
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Jive.jar
* libhdbpp-java.jar
* JTango.jar
* ATKCore.jar
* ATKWidget.jar
  

#### Toolchain Dependencies 

* javac 8 or higher
* maven
  

### Build


Instructions on building the project.

```
cd CassandraTools
mvn package
```

