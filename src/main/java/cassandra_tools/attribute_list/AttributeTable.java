//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// Pascal Verdier: pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2018,2019
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package cassandra_tools.attribute_list;

import cassandra_tools.commons.HdbAttribute;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/*    Add in Parent source file:

*/

/**
 * This class is able to display data on a table
 *
 * @author verdier
 */

public class AttributeTable extends JTable {
    private List<Attribute> attributeList = new ArrayList<>();
    private int tableWidth = 0;
    private int selectedRow = -1;
    private boolean allSelected = true;

    private static final int[] COLUMN_WIDTH = { 600, 50};
    private static final String[] COLUMN_HEADERS = {
            "Attribute Names", "Selection",
    };
    private static final int ATTRIBUTE = 0;
    private static final int SELECTION = 1;
    private static final Color firstColumnColor = new Color(0xdd, 0xdd, 0xdd);
    //===============================================================
    //===============================================================
    private static class Attribute {
        private String name;
        private boolean selected = true;
        private Attribute(String name) { this.name = name; }
    }
    //===============================================================
    //===============================================================
    public AttributeTable(List<HdbAttribute> hdbAttributes) {
        for (HdbAttribute hdbAttribute : hdbAttributes)
            this.attributeList.add(new Attribute(hdbAttribute.getFullName()));
        // Create the table
        DataTableModel model = new DataTableModel();
        setModel(model);
        setRowSelectionAllowed(true);
        setDefaultRenderer(String.class, new LabelCellRenderer());
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                tableActionPerformed(event);
            }
        });
        //  Column header management
        getTableHeader().setFont(new Font("Dialog", Font.BOLD, 14));
        getTableHeader().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                headerActionPerformed(event);
            }
        });

        //  Set column width
        final Enumeration columnEnum = getColumnModel().getColumns();
        int i = 0;
        TableColumn tableColumn;
        while (columnEnum.hasMoreElements()) {
            tableWidth += COLUMN_WIDTH[i];
            tableColumn = (TableColumn) columnEnum.nextElement();
            tableColumn.setPreferredWidth(COLUMN_WIDTH[i++]);
        }
    }
    //===============================================================
    //===============================================================
    private void headerActionPerformed(MouseEvent event) {
        if (columnAtPoint(new Point(event.getX(), event.getY()))==SELECTION) {
            allSelected = !allSelected;
            for (Attribute attribute : attributeList)
                attribute.selected = allSelected;
            repaint();
        }
    }
    //===============================================================
    //===============================================================
    private void tableActionPerformed(MouseEvent event) {
        selectedRow = rowAtPoint(new Point(event.getX(), event.getY()));
        if (event.getButton()==1) {
            if (columnAtPoint(new Point(event.getX(), event.getY()))==SELECTION) {
                Attribute attribute = attributeList.get(selectedRow);
                attribute.selected = !attribute.selected;
            }
            repaint();
        }
    }
    //===============================================================
    //===============================================================
    public List<String> getSelection() {
        List<String> list = new ArrayList<>();
        for (Attribute attribute : attributeList) {
            if (attribute.selected)
                list.add(attribute.name);
        }
        return list;
    }
    //===============================================================
    //===============================================================
    public int getTableWidth() {
        return tableWidth;
    }
    //===============================================================
    //===============================================================


    //==============================================================
    /**
     * The Table model
     */
    //==============================================================
    public class DataTableModel extends AbstractTableModel {
        //==========================================================
        @Override
        public int getColumnCount() {
            return COLUMN_HEADERS.length;
        }
        //==========================================================
        @Override
        public int getRowCount() {
            return attributeList.size();
        }
        //==========================================================
        @Override
        public String getColumnName(int columnIndex) {
            if (columnIndex >= getColumnCount())
                return COLUMN_HEADERS[getColumnCount() - 1];
            else
                return COLUMN_HEADERS[columnIndex];
        }
        //==========================================================
        @Override
        public Object getValueAt(int row, int column) {
            if (column == SELECTION)
                return attributeList.get(row).selected;
            //  Done by renderer
            return "";
        }
        //==========================================================
        @Override
        public void setValueAt(Object value, int row, int column) {
        }
        //==========================================================
        /**
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         *
         * @param column the specified co;umn number
         * @return the cell class at first row for specified column.
         */
        //==========================================================
        @Override
        public Class getColumnClass(int column) {
            if (isVisible()) {
                if (column == SELECTION)
                    return Boolean.class;
                else
                    return getValueAt(0, column).getClass();
            }
            else
                return null;
        }
        //==========================================================
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
        //==========================================================
    }
    //==============================================================
    //==============================================================


    //==============================================================
    /**
     * Renderer to set cell color
     */
    //==============================================================
    public class LabelCellRenderer extends JLabel implements TableCellRenderer {
        //==========================================================
        public LabelCellRenderer() {
            //setFont(new Font("Dialog", Font.BOLD, 11));
            setOpaque(true); //MUST do this for background to show up.
        }
        //==========================================================
        @Override
        public Component getTableCellRendererComponent(
                JTable table, Object value,
                boolean isSelected, boolean hasFocus,
                int row, int column) {
            Attribute attribute = attributeList.get(row);
            setIcon(null);

            //  Display value
            setBackground(Color.white);
            if (column==ATTRIBUTE) {
                    setText(" " + attribute.name);
                    if (row==selectedRow)
                        setBackground(selectionBackground);
                    else
                        setBackground(firstColumnColor);
            }
            return this;
        }
    }
    //==============================================================
    //==============================================================
}
