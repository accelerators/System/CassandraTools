//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  java source code for main swing class.
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,......,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//-======================================================================

package cassandra_tools.attribute_list;

import cassandra_tools.commons.CassandraConnection;
import cassandra_tools.commons.HdbAttribute;
import cassandra_tools.commons.SplashUtils;
import cassandra_tools.commons.Utils;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

//=======================================================
/**
 *	JFrame Class to display info
 *
 * @author  Pascal Verdier
 */
//=======================================================
public class HdbAttributeListFrame extends JFrame {
	private static JFileChooser  chooser = null;
	private AttributeTable attributeTable = null;
	//=======================================================
    /**
	 *	Creates new form HdbAttributeListFrame
	 */
	//=======================================================
    public HdbAttributeListFrame(String wildcard) {
        initComponents();
        if (wildcard!=null && !wildcard.isEmpty()) {
            wildcardTextField.setText(wildcard);
            searchDisplayAttributeMatching();
        }

        pack();
        ATKGraphicsUtils.centerFrameOnScreen(this);
	}
	//=======================================================
	//=======================================================
    private void searchDisplayAttributeMatching() {
        try {
            // get wildcard
            SplashUtils.getInstance().startSplash();
            SplashUtils.getInstance().setSplashProgress(20, "Reading HDB++");
            String wildcard = wildcardTextField.getText();

            // Reading HDB++ and Filtering attributes
            List<HdbAttribute> attributeList = getAttributeList(wildcard);
            infoLabel.setText(attributeList.size() +
                    " attribute(s) found in " + System.getenv("HDB_TYPE") + " hdb");

            // Build the attribute table
            SplashUtils.getInstance().setSplashProgress(40, "Building the attribute table");
            if (attributeTable!=null)
                getContentPane().remove(attributeTable.getParent().getParent());
            attributeTable = new AttributeTable(attributeList);
            JScrollPane scrollPane = new JScrollPane(attributeTable);
            scrollPane.setPreferredSize(new Dimension(attributeTable.getTableWidth(), 500));
            getContentPane().add(scrollPane, BorderLayout.CENTER);
            SplashUtils.getInstance().stopSplash();
        }
        catch (DevFailed e) {
            SplashUtils.getInstance().stopSplash();
            ErrorPane.showErrorMessage(this, null, e);
        }
    }
	//=======================================================
	//=======================================================
    private List<HdbAttribute> getAttributeList(String wildcard) throws DevFailed {
        //  Get attribute list
        List<HdbAttribute> attributeList = new ArrayList<>();
        String hdbType = System.getenv("HDB_TYPE");
        if (hdbType==null)
            Except.throw_exception("PropertyNotSet", "HDB_TYPE environment not set");

        if (hdbType.equalsIgnoreCase("cassandra")) {
            CassandraConnection.getInstance().updateArchiveAttributes();
            attributeList = CassandraConnection.getInstance().getHdbAttributeList();
        }
        else {
            Except.throw_exception("BadParameter", "HDB " + hdbType + " not implemented yet");
            /*
            TimeScaleConnection.getInstance().updateArchiveAttributes();
            attributeList = TimeScaleConnection.getInstance().getHdbAttributeList();
            */
        }

        //  Get only attribute matching wildcard
        List<HdbAttribute> list = new ArrayList<>();
        Pattern pattern = getPattern(wildcard);
        for (HdbAttribute attribute : attributeList) {
            if (attribute.getFullName().matches(pattern.pattern()))
                list.add(attribute);
        }
        return list;
    }
	//=======================================================
	//=======================================================
    private Pattern getPattern(String wildcard) {
        return Pattern.compile(Utils.strReplace(wildcard, "*", ".*"));
    }
	//=======================================================
	//=======================================================

	//=======================================================
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
	//=======================================================
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel topPanel = new javax.swing.JPanel();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        wildcardTextField = new javax.swing.JTextField();
        javax.swing.JButton searchButton = new javax.swing.JButton();
        javax.swing.JPanel bottomPanel = new javax.swing.JPanel();
        infoLabel = new javax.swing.JLabel();
        javax.swing.JMenuBar menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem saveItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem jMenuItem1 = new javax.swing.JMenuItem();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });
        getContentPane().setLayout(new java.awt.BorderLayout());

        jLabel1.setText("Wildcard:  ");
        topPanel.add(jLabel1);

        wildcardTextField.setColumns(30);
        wildcardTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wildcardTextFieldActionPerformed(evt);
            }
        });
        topPanel.add(wildcardTextField);

        searchButton.setText(" Search ");
        searchButton.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });
        topPanel.add(searchButton);

        getContentPane().add(topPanel, java.awt.BorderLayout.PAGE_START);

        infoLabel.setText("            ");
        bottomPanel.add(infoLabel);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");

        saveItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveItem.setMnemonic('S');
        saveItem.setText("Save");
        saveItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveItem);

        exitItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitItem.setMnemonic('E');
        exitItem.setText("Exit");
        exitItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitItem);

        menuBar.add(fileMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("help");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setMnemonic('A');
        jMenuItem1.setText("About");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItem1);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void saveItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveItemActionPerformed
		if (chooser==null) {
			chooser = new JFileChooser(new File("").getAbsolutePath());
			chooser.setApproveButtonText("Save");
			chooser.setApproveButtonToolTipText("Save attribute list");
		}

		if (chooser.showOpenDialog(this)==JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			if (file!=null) {
			    try {
                    if (file.exists()) {
                        if (JOptionPane.showConfirmDialog(this,
                                file.toString() + "\nAlready exists !\n\nOverwrite ?",
                                "Confirm", JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION)
                            return;
                    }
                    String filename = file.getAbsolutePath();
                    List<String> attributeNames = attributeTable.getSelection();
                    StringBuilder sb = new StringBuilder();
                    for (String  attributeName : attributeNames)
                        sb.append(attributeName).append('\n');
                    Utils.writeFile(filename, sb.toString());
                }
			    catch (DevFailed e) {
			        ErrorPane.showErrorMessage(this, null, e);
                }
			}
		}
    }//GEN-LAST:event_saveItemActionPerformed
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void exitItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitItemActionPerformed
        doClose();
    }//GEN-LAST:event_exitItemActionPerformed
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        doClose();
    }//GEN-LAST:event_exitForm
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        String  message = "This application is able to bla bla\n" +
                "\nPascal Verdier - Accelerator Control Unit";
        JOptionPane.showMessageDialog(this, message, "Help Window", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem1ActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void wildcardTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wildcardTextFieldActionPerformed
        searchDisplayAttributeMatching();
    }//GEN-LAST:event_wildcardTextFieldActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        // TODO add your handling code here:
        searchDisplayAttributeMatching();
    }//GEN-LAST:event_searchButtonActionPerformed
	//=======================================================
	//=======================================================
    private void doClose() {
        System.exit(0);
    }
	//=======================================================
	//=======================================================



	//=======================================================
    /**
     * @param args the command line arguments
     */
	//=======================================================
    public static void main(String[] args) {
        String wildcard = "*";
        if (args.length>0)
            wildcard = args[0];
   		new HdbAttributeListFrame(wildcard).setVisible(true);
    }
	//=======================================================
	//=======================================================





	//=======================================================
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel infoLabel;
    private javax.swing.JTextField wildcardTextField;
    // End of variables declaration//GEN-END:variables
	//=======================================================

}
