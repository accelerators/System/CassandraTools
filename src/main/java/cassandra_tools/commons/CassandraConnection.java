//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhdbextract/src/org/tango/jhdbextract/commons/MySqlConnection.java,v $
//
// Project:   Tango
//
// Description:  java source code for Tango tool..
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision: 1.1.1.1 $
//
//-======================================================================

package cassandra_tools.commons;


import com.datastax.driver.core.*;
import com.datastax.driver.core.exceptions.DriverException;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;

import java.util.*;

/**
 * This class is a singleton defining a Cassandra database connection
 * Created: VERDIER
 * Date: 10/10/14
 */
public class CassandraConnection {

    private Cluster cluster;
    private Session session;
    private Hashtable<String, HdbAttribute> attributeMap = new Hashtable<>();

    private static String[] contactPoints;
    private static CassandraConnection instance = null;
    private static final boolean writeHDB = true;
    //===============================================================
    //===============================================================
    private CassandraConnection() throws DevFailed {

        //  Get contact points from environment variable
        String str = System.getenv("HDB_CONTACT_POINTS");
        if (str!=null && !str.isEmpty()) {
            StringTokenizer stk = new StringTokenizer(str, ",");
            contactPoints = new String[stk.countTokens()];
            int i=0;
            while (stk.hasMoreTokens())
                contactPoints[i++] = stk.nextToken();
        }
        else
            Except.throw_exception("PropertyNotDefined", "HDB_CONTACT_POINTS is not defined");


        //  Build cluster with contact points
        Cluster.Builder builder = Cluster.builder();
        for (String contactPoint : contactPoints) {
            builder.addContactPoint(contactPoint);
        }
        cluster = builder.build();

        //  Set protocol
        cluster.getConfiguration()
                .getProtocolOptions()
                .setCompression(ProtocolOptions.Compression.LZ4);
        //  Build session on database
        String hdbName = "hdb";
        str = System.getenv("HDB_NAME");
        if (str!=null && !str.isEmpty())
            hdbName = str;
        session = cluster.connect(hdbName);
    }
    //===============================================================
    //===============================================================
    public static CassandraConnection getInstance() throws DevFailed {
        if (instance==null) {
            instance = new CassandraConnection();
        }
        return instance;
    }
    //===============================================================
    //===============================================================
    public void updateArchiveAttributes() throws DevFailed {
        String query = "SELECT * FROM att_conf";
        ResultSet resultSet;
        attributeMap.clear();
        try {
            resultSet = session.execute(query);
            for (Row row : resultSet) {
                String csName = row.getString("cs_name");
                String attName = row.getString("att_name");
                UUID uuid = row.getUUID("att_conf_id");
                String tableName = row.getString("data_type");
                int ttl = row.getInt("ttl");

                HdbAttribute attribute = new HdbAttribute(csName, attName, uuid, tableName, ttl);
                attributeMap.put(attribute.getFullName(), attribute);
            }
        } catch (DriverException e) {
            Except.throw_exception("ReadDataFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    public void addNewAttribute(String srcAttributeName, String newAttributeName) throws DevFailed {
        //  Check if new attribute is defined in TANGO database
        String deviceName = newAttributeName.substring(0, newAttributeName.lastIndexOf('/'));
        new DeviceProxy(deviceName);

        //  Check if new attribute already exists in HDB++
        HdbAttribute newAttribute = getHdbAttribute(newAttributeName);
        if (newAttribute!=null) {
            Except.throw_exception("BadParam", "NEW attribute:\n" +
                    newAttributeName + "\n      already exists in HDB++ table  " + newAttribute.getTableName());
        }
        //  Get original attribute
        HdbAttribute attribute = getHdbAttribute(srcAttributeName);
        if (attribute==null) {
            Except.throw_exception("BadParam", "SRC attribute:\n" + srcAttributeName + "\n     not found in HDB++");
        }
        System.out.println(attribute);

        // Replace by new TANGO_HOST and new attribute name
        String tangoHost = Utils.getTangoHost(newAttributeName);
        String attributeName = Utils.getAttributeName(newAttributeName);
        attribute.setCsName(tangoHost);
        attribute.setAttributeName(attributeName);
        System.out.println(attribute);

        //  And this new attribute to tables
        if (writeHDB) {
            addToAttConf(attribute);
            addToDomains(attribute);
            addToFamilies(attribute);
            addToMembers(attribute);
            addToAttNames(attribute);
        }
        else // simulate
            try { Thread.sleep(500); } catch (InterruptedException e) { /* */ }

        //  OK -> add it to the map
        attributeMap.put(attribute.getFullName(), attribute);
        System.out.println(attribute.getFullName() + "  added");
    }
    //===============================================================
    //===============================================================
    public void addToAttConf(HdbAttribute newAttribute) throws DevFailed {
        //  Add it in att_conf
        RegularStatement statement = new SimpleStatement(
                "INSERT INTO att_conf(cs_name,att_name,att_conf_id,data_type,ttl) " +
                " VALUES('" + newAttribute.getCsName() +
                "','" + newAttribute.getAttributeName() +
                "'," + newAttribute.getUUID() +
                ",'" + newAttribute.getTableName() +
                "'," + newAttribute.getTTL() + ")");
        try {
            statement.setConsistencyLevel(ConsistencyLevel.ALL);
            session.execute(statement);
            System.out.println("AttConf done !");
        } catch (DriverException e) {
            Except.throw_exception("InsertDataFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    private void addToDomains(HdbAttribute hdbAttribute) throws DevFailed {
        RegularStatement statement = new SimpleStatement(
                "INSERT INTO domains(cs_name,domain) " +
                        " VALUES('" + hdbAttribute.getCsName() +
                        "','" + hdbAttribute.getDomainField() + "')");

        try {
            statement.setConsistencyLevel(ConsistencyLevel.ALL);
            session.execute(statement);
            System.out.println("Domain done !");
        } catch (DriverException e) {
            Except.throw_exception("InsertDataFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    private void addToFamilies(HdbAttribute hdbAttribute) throws DevFailed {
        RegularStatement statement = new SimpleStatement(
                "INSERT INTO families(cs_name,domain,family) " +
                        " VALUES('" + hdbAttribute.getCsName() +
                        "','" + hdbAttribute.getDomainField() +
                        "','" + hdbAttribute.getFamilyFiled() +
                        "')");

        try {
            statement.setConsistencyLevel(ConsistencyLevel.ALL);
            session.execute(statement);
            System.out.println("Families done !");
        } catch (DriverException e) {
            Except.throw_exception("InsertDataFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    private void addToMembers(HdbAttribute hdbAttribute) throws DevFailed {
        RegularStatement statement = new SimpleStatement(
                "INSERT INTO members(cs_name,domain,family,member) " +
                        " VALUES('" + hdbAttribute.getCsName() +
                        "','" + hdbAttribute.getDomainField() +
                        "','" + hdbAttribute.getFamilyFiled() +
                        "','" + hdbAttribute.getMemberField() +
                        "')");

        try {
            statement.setConsistencyLevel(ConsistencyLevel.ALL);
            session.execute(statement);
            System.out.println("Members done !");
        } catch (DriverException e) {
            Except.throw_exception("InsertDataFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    private void addToAttNames(HdbAttribute hdbAttribute) throws DevFailed {
        RegularStatement statement = new SimpleStatement(
                "INSERT INTO att_names(cs_name,domain,family,member,name) " +
                        " VALUES('" + hdbAttribute.getCsName() +
                        "','" + hdbAttribute.getDomainField() +
                        "','" + hdbAttribute.getFamilyFiled() +
                        "','" + hdbAttribute.getMemberField() +
                        "','" + hdbAttribute.getAttributeField() +
                        "')");

        try {
            statement.setConsistencyLevel(ConsistencyLevel.ALL);
            session.execute(statement);
            System.out.println("AttNames done !");
        } catch (DriverException e) {
            Except.throw_exception("InsertDataFailed", e.getMessage());
        }
    }
    //===============================================================
    //===============================================================
    public HdbAttribute getHdbAttribute(String attributeName) throws DevFailed {
        return attributeMap.get(attributeName);
    }
    //===============================================================
    //===============================================================
    public List<HdbAttribute> getHdbAttributeList() throws DevFailed {
        if (attributeMap.isEmpty())
            updateArchiveAttributes();
        List<HdbAttribute> list = new ArrayList<>();
        Set<String> keys = attributeMap.keySet();
        for (String key : keys) {
            HdbAttribute attribute = attributeMap.get(key);
            if (attribute!=null)
                list.add(attribute);
        }
        list.sort(new AttributeComparator());
        return list;
    }
    //===============================================================
    //===============================================================
    @SuppressWarnings("unused")
    public String getConnectionInformation() {
        Metadata metadata = cluster.getMetadata();
        StringBuilder sb = new StringBuilder();
        sb.append("Cassandra Database cluster: ").append(metadata.getClusterName()).append('\n');
        for ( Host host : metadata.getAllHosts() ) {
            sb.append("  - ").append(host.getDatacenter())
                    .append(": ").append(host.getAddress().getCanonicalHostName())
                    .append("  in  ").append(host.getRack())
                    .append("  is ").append((host.isUp())? "Up" : "Down")
                    .append('\n');
        }
        return sb.toString().trim();
    }
    //===============================================================
    //===============================================================
    public String toString() {
        return contactPoints[0];
    }
    //===============================================================
    //===============================================================
    //  DELETE from att_conf WHERE cs_name="accelerators.esrf.fr:10000" AND att_name="/infra/hqps-common/master/inputactivepower";
    //===============================================================
    //===============================================================




    //======================================================
    /**
     * Comparators class to sort by name
     */
    //======================================================
    public static class AttributeComparator implements Comparator<HdbAttribute> {
        //==================================================
        public int compare(HdbAttribute attribute1, HdbAttribute attribute2) {
            if (attribute1==null)
                return 1;
            else if (attribute2==null)
                return -1;
            else
                return attribute1.getFullName().compareTo(attribute2.getFullName());
        }
        //==================================================
    }
    //======================================================
    //======================================================
}
