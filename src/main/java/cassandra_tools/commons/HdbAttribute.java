//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhdbextract/src/org/tango/jhdbextract/commons/ArchiveAttribute.java,v $
//
// Project:   Tango
//
// Description:  java source code for Tango tool..
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision: 1.6 $
//
//-======================================================================

package cassandra_tools.commons;

import java.util.List;
import java.util.UUID;

/**
 *  Defines an attribute stored in HDB++.
 *  It is a att_conf table row.
 */
public class HdbAttribute {
    private String csName;
    private String attributeName;
    private String tableName;
    private UUID uuid;
    private int ttl;
    private List<String> attributeComponent;
    private static final int DOMAIN = 0;
    private static final int FAMILY = 1;
    private static final int MEMBER = 2;
    private static final int ATTRIBUTE = 3;
    //===============================================================
    //===============================================================
    public HdbAttribute(String csName, String attributeName, UUID uuid, String tableName, int ttl) {
        this.csName = csName;
        this.attributeName = attributeName;
        this.uuid = uuid;
        this.tableName = tableName;
        this.ttl = ttl;
        attributeComponent = Utils.getAttributeFields(attributeName);
    }
    //===============================================================
    //===============================================================
    public static String buildFullAttributeName(String controlSystemName, String attributeName) {
        return "tango://" + controlSystemName + "/" + attributeName;
    }
    //===============================================================
    //===============================================================
    public void setCsName(String csName) {
        this.csName = csName;
    }
    //===============================================================
    //===============================================================
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
        attributeComponent = Utils.getAttributeFields(attributeName);
    }
    //===============================================================
    //===============================================================
    public String getCsName() {
        return csName;
    }
    //===============================================================
    //===============================================================
    public String getAttributeName() {
        return attributeName;
    }
    //===============================================================
    //===============================================================
    public UUID getUUID() {
        return uuid;
    }
    //===============================================================
    //===============================================================
    public int getTTL() {
        return ttl;
    }
    //===============================================================
    //===============================================================
    public String getTableName() {
        return tableName;
    }
    //===============================================================
    //===============================================================
    public String getFullName() {
        return buildFullAttributeName(csName, attributeName);
    }
    //===============================================================
    //===============================================================
    public String getDomainField() {
        return attributeComponent.get(DOMAIN);
    }
    //===============================================================
    //===============================================================
    public String getFamilyFiled() {
        return attributeComponent.get(FAMILY);
    }
    //===============================================================
    //===============================================================
    public String getMemberField() {
        return attributeComponent.get(MEMBER);
    }
    //===============================================================
    //===============================================================
    public String getAttributeField() {
        return attributeComponent.get(ATTRIBUTE);
    }
    //===============================================================
    //===============================================================
    public String toToolTip() {
        return  Utils.buildToolTip(getFullName() +
                "\n TTL:  " + ttl  +
                "\n tableName:  "  + tableName +
                "\n   UUID:  " + uuid);
    }
    //===============================================================
    //===============================================================
    public String toString() {
        return getAttributeField();
    }
    //===============================================================
    //===============================================================
}
