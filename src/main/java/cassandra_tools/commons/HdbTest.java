//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2017,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package cassandra_tools.commons;

import fr.esrf.Tango.DevFailed;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;


/**
 * This class is able to
 *
 * @author verdier
 */

public class HdbTest {
    //===============================================================
    //===============================================================
    public HdbTest(String srcAttributeName, String newAttributeName) throws DevFailed {
        CassandraConnection.getInstance().addNewAttribute(srcAttributeName, newAttributeName);
    }
    //===============================================================
    //===============================================================
    //===============================================================
    //===============================================================

    //===============================================================
    //===============================================================
    public static void main(String[] args) {

        String srcAttributeName = "tango://orion.esrf.fr:10000/sys/hqps-common/master/inputactivepower";
        String newAttributeName = "tango://accelerators.esrf.fr:10000/infra/hqps-common/master/inputactivepower";

        try {
            new HdbTest(srcAttributeName, newAttributeName);
        } catch (DevFailed e) {
            //Except.print_exception(e);
            ErrorPane.showErrorMessage(new JFrame(), null, e);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
