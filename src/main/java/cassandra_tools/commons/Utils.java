//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//-======================================================================

package cassandra_tools.commons;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.*;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * This class is a set of static utility methods
 *
 * @author verdier
 */

@SuppressWarnings("WeakerAccess")
public class Utils {
    private static Utils instance = new Utils();
    //===============================================================
    //===============================================================
    public static Utils getInstance() {
        return instance;
    }
    //===============================================================
    //===============================================================
    public String getApplicationRelease() {
        String applicationName = getClass().getPackage().getImplementationTitle();
        if (applicationName == null)
            applicationName = "CassandraTools";

        String release = getClass().getPackage().getImplementationVersion();
        if (release!=null)
            return applicationName + "-" + release;
        else
           return applicationName + " - not released";
    }
    //======================================================================
    //======================================================================
    public static String getTangoHost() throws DevFailed {
        String tangoHost = ApiUtil.getTangoHost();
        String realTangoHost = ApiUtil.get_db_obj(tangoHost).get_tango_host();
        //  tangoHost could be an alias
        // realTangoHost has FQDN
        int idx = realTangoHost.lastIndexOf(':');
        String port = realTangoHost.substring(idx);
        String fqdn = realTangoHost.substring(realTangoHost.indexOf('.'), idx);
        String host = tangoHost.substring(0, tangoHost.indexOf(':'));

        return host + fqdn + port;
    }
    //======================================================================
    //======================================================================
    public static String lines2text(List<String> lines) {
        StringBuilder sb = new StringBuilder();
        for (String line : lines)
            sb.append(line).append('\n');
        return sb.toString().trim();
    }
    //======================================================================
    //======================================================================
    public static List<String> text2lines(String text) {
        List<String> list = new ArrayList<>();
        StringTokenizer stk = new StringTokenizer(text);
        while(stk.hasMoreTokens())
            list.add(stk.nextToken());
        return list;
    }
    //======================================================================
    private static final String ATT_HEADER = "tango://";
    //======================================================================
    public static List<String> replaceTangoHost(List<String> inList, String tangoHost) {
        List<String> outList = new ArrayList<>();
        for (String item : inList) {
            if (item.startsWith(ATT_HEADER)) {
                int idx = item.indexOf('/', ATT_HEADER.length());
                if (idx>0) {
                    outList.add(ATT_HEADER+tangoHost+item.substring(idx));
                }
            }
        }
        return outList;
    }
    //======================================================================
    //======================================================================
    public static List<String> replaceDomain(List<String> inList, String domain) {
        List<String> outList = new ArrayList<>();
        for (String item : inList) {
            if (item.startsWith(ATT_HEADER)) {
                int start = item.indexOf('/', ATT_HEADER.length());
                if (start>0) {
                    int end = item.indexOf('/', ++start);
                    if (end>0) {
                        outList.add(item.substring(0, start) + domain + item.substring(end));
                    }
                }
            }
        }
        return outList;
    }
    //======================================================================
    //======================================================================
    public static String getAttributeName(String fullAttributeName) {
        if (fullAttributeName.startsWith(ATT_HEADER)) {
            int start = fullAttributeName.indexOf('/', ATT_HEADER.length());
            if (start>0) {
                return fullAttributeName.substring(++start);
            }
        }
        return null;
    }
    //======================================================================
    //======================================================================
    public static String getTangoHost(String fullAttributeName) {
        if (fullAttributeName.startsWith(ATT_HEADER)) {
            int idx = fullAttributeName.indexOf('/', ATT_HEADER.length());
            if (idx>0)
                return fullAttributeName.substring(ATT_HEADER.length(), idx);
        }
        return null;
    }
    //======================================================================
    //======================================================================


    //===============================================================
    //===============================================================
    public static String checkReturnNumber(String message) {
        int nbReturnMax = 60;
        int nbReturns = 0;
        int idx = 0;
        int end = 0;
        while (nbReturns<nbReturnMax && (idx=message.indexOf('\n', ++idx))>0) {
            end = idx;
            nbReturns++;
        }
        if (end<message.length())
            return message.substring(0, end) + "\n     - - - -";
        else
            return message;
    }
    //===============================================================
    //===============================================================
    public static String strReplace(String text, String old_str, String new_str) {
        if (text == null) return "";
        for (int pos = 0; (pos = text.indexOf(old_str, pos)) >= 0; pos += new_str.length())
            text = text.substring(0, pos) + new_str +
                    text.substring(pos + old_str.length());
        return text;
    }
    //===============================================================
    //===============================================================
    public static String replaceChar(String str, char src, char target) {
        int idx;
        while ((idx=str.indexOf(src))>0)
            str = str.substring(0, idx) + target + str.substring(++idx);
        return str;
    }
    //===============================================================
    //===============================================================
    public static List<String> getAttributeFields(String attributeName) {
        StringTokenizer stk = new StringTokenizer(attributeName, "/");
        List<String> list = new ArrayList<>();
        while (stk.hasMoreTokens())
            list.add(stk.nextToken());
        return list;
    }
    //===============================================================
    //===============================================================
    public static int getLongestLineLength(List<String> lines) {
        return getLongestLine(lines.toArray(new String[0])).length();
    }
    //===============================================================
    //===============================================================
    public static String getLongestLine(String[] lines) {
        String  longest = "";
        for (String line : lines)
            if (line.length()>longest.length())
                longest = line;
        return longest;
    }
    //===============================================================
    /**
     * Format with time before date, and no millis
     * @param ms date to format
     * @return the formatted date
     */
    //===============================================================
    public static String formatDate(long ms, boolean withMillis) {
        //  Format the date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY  HH:mm:ss");
        String date = simpleDateFormat.format(new Date(ms));

        //  Compute and add milliseconds
        double doubleTime = (double)ms/1000;
        ms = (long)((doubleTime - (long)doubleTime)*1000);
        String millis = String.format("%03d", ms);

        if (withMillis)
            date += '.' + millis;
        return date;
    }
    //===============================================================
    /**
     * Format with date before time for file name
     * @return the formatted date
     */
    //===============================================================
    public static String formatFileDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd_HH-mm");
        return simpleDateFormat.format(new Date(System.currentTimeMillis()));
    }
    //===============================================================
    /**
     * Open a file and return text read as a list of lines.
     *
     * @param filename file to be read.
     * @return the file content read as a list of lines.
     * @throws DevFailed in case of failure during read file.
     */
    //===============================================================
    public static List<String> readFileLines(String filename) throws DevFailed {
        String code = readFile(filename);
        List<String> lines = new ArrayList<>();
        StringTokenizer stk = new StringTokenizer(code, "\n");
        while (stk.hasMoreTokens())
            lines.add(stk.nextToken().trim());
        return lines;
    }
    //===============================================================
    /**
     * Open a file and return text read.
     *
     * @param filename file to be read.
     * @return the file content read.
     * @throws DevFailed in case of failure during read file.
     */
    //===============================================================
    public static String readFile(String filename) throws DevFailed {
        String str = "";
        try {
            FileInputStream inputStream = new FileInputStream(filename);
            int nb = inputStream.available();
            byte[] buffer = new byte[nb];
            nb = inputStream.read(buffer);
            inputStream.close();
            if (nb > 0)
                str = new String(buffer);
        } catch (Exception e) {
            Except.throw_exception(e.getMessage(), e.toString());
        }
        return str;
    }
    //===============================================================
    //===============================================================
    public static void writeFile(String fileName, String code) throws DevFailed {
        try {
            FileOutputStream fid = new FileOutputStream(fileName);
            fid.write(code.getBytes());
            fid.close();
        } catch (Exception e) {
            Except.throw_exception(e.getMessage(), e.toString());
        }
    }
    //===============================================================
    //===============================================================
    public static void writeFile(String fileName, List<String> list) throws DevFailed {
        StringBuilder sb = new StringBuilder();
        for (String item : list)
            sb.append(item).append('\n');
        writeFile(fileName, sb.toString());
    }
    //===============================================================
    //===============================================================
    @SuppressWarnings("unused")
    public static void restartServer(DeviceProxy proxy) throws DevFailed {
        DeviceInfo info = proxy.get_info();
        String serverName = info.server;
        String hostName = info.hostname;
        if (hostName.indexOf('.')>0)
            hostName = hostName.substring(0, hostName.indexOf('.'));
        String starterName = "tango/admin/" + hostName;
        System.out.println("Execute a restart "+ serverName + " command on " + starterName);

        DeviceProxy starter = new DeviceProxy(starterName);
        DeviceData argIn = new DeviceData();
        argIn.insert(serverName);
        starter.command_inout("HardKillServer", argIn);

        try { Thread.sleep(5000); } catch (InterruptedException e) { /* */ }
        starter.command_inout("DevStart", argIn);
    }
    //===============================================================
    /**
     *  Build a simple dialog with component at center position
     * @param parent dialog parent
     * @param modal  true if dialog must be modal
     * @param title  dialog title
     * @param component component to be added at center
     * @return the dialog
     */
    //===============================================================
    public static JDialog buildDialog(JFrame parent, boolean modal, String title, Component component) {
        final JDialog dialog = new JDialog(parent, modal);
        JPanel dialogPanel = new JPanel(new BorderLayout());
        dialogPanel.add(component, BorderLayout.CENTER);
        JButton button = new JButton("Dismiss");
        button.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                dialog.setVisible(false);
                dialog.dispose();
            }
        });
        JPanel panel = new JPanel();
        panel.add(button);
        dialogPanel.add(panel, BorderLayout.SOUTH);
        if (title!=null) {
            JLabel label = new JLabel(title);
            label.setFont(new Font("dialog", Font.BOLD, 18));
            panel = new JPanel();
            panel.add(label);
            dialogPanel.add(panel, BorderLayout.NORTH);
        }
        dialog.setContentPane(dialogPanel);
        dialog.pack();

        return dialog;
    }
    //===============================================================
    //===============================================================
    public static boolean osIsUnix() {
        return !osIsWindows();
    }
    //===============================================================
    //===============================================================
    public static boolean osIsWindows() {
        String os = System.getProperty("os.name");
        return os.toLowerCase().startsWith("windows");
    }
    //===============================================================
    //===============================================================
    /**
     * Execute a shell command and throw exception if command failed.
     *
     * @param cmd shell command to be executed.
     * @throws DevFailed in case of execution failed
     */
    //===============================================================
    public static void executeShellCmdAndReturn(String cmd) throws DevFailed {
        try {
            Process process = Runtime.getRuntime().exec(cmd);

            // get command output stream and
            // put a buffered reader input stream on it.
            InputStream inputStream = process.getInputStream();
            new BufferedReader(new InputStreamReader(inputStream));

            // do not read output lines from command
            // Do not check its exit value
        }
        catch (IOException e) {
            Except.throw_exception("IOException", e.getMessage());
        }
    }
    //===============================================================
    private static jive3.MainPanel jive = null;
    //===============================================================
    public static void startJive(String name, boolean forDevice) {
        //  Start jive and go to the device node
        if (jive==null) {
            jive = new jive3.MainPanel(false, false);
        }
        jive.setVisible(true);
        if (forDevice)
            jive.goToDeviceNode(name);
        else
            jive.goToServerNode(name);
    }
    //======================================================================
    //======================================================================
    public static void testDevice(Component parent, String deviceName) throws DevFailed {
        JDialog dialog;
        if (parent instanceof JFrame)
            dialog = new JDialog((JFrame)parent, false);
        else
            dialog = new JDialog((JDialog)parent, false);
        dialog.setTitle(deviceName + " Device Panel");
        dialog.setContentPane(new jive.ExecDev(deviceName));
        dialog.pack();
        dialog.setVisible(true);
        ATKGraphicsUtils.centerDialog(dialog);
    }
    //===================================================================
    //===================================================================



    //  Multi lines tooltip methods
    private static final String tooltipHeader =
            "<html><BODY TEXT=\"#000000\" BGCOLOR=\"#FFFFD0\">";

    //===============================================================
    //===============================================================
    @SuppressWarnings("UnusedDeclaration")
    public static String buildToolTip(String text) {
        return buildToolTip(null, text);
    }

    //===============================================================
    //===============================================================
    public static String buildToolTip(String title, String text) {

        StringBuilder sb = new StringBuilder(tooltipHeader);
        if (title != null && title.length() > 0) {
            sb.append("<b><Center>")
                    .append("&nbsp;&nbsp;")
                    .append(strReplace(title, "\n", "&nbsp;&nbsp;<br>&nbsp;&nbsp;\n"))
                    .append("&nbsp;&nbsp;")
                    .append("</center></b><HR WIDTH=\"100%\">");
        }
        if (text != null && text.length() > 0) {
            text = Utils.strReplace(text, "\n", "&nbsp;&nbsp;<br>\n&nbsp;&nbsp;");
            sb.append("&nbsp;&nbsp;")
                    .append(Utils.strReplace(text, "\' \'", "\'&nbsp;&nbsp;\'")).append("&nbsp;&nbsp;");
        }
        return sb.toString();
    }
    //===============================================================
    //===============================================================
}
