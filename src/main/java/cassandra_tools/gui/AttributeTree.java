//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:	java source code for display JTree
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2018,2019
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// : 1.2 $
//
// :  $
//
//-======================================================================

package cassandra_tools.gui;

import cassandra_tools.commons.HdbAttribute;
import fr.esrf.TangoDs.TangoConst;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.util.*;
import java.util.List;


/**
 * This class is able to display structure in a tree.
 */

public class AttributeTree extends JTree implements TangoConst {
    private DefaultMutableTreeNode rootNode;

    private static final int CS = 0;
    private static final int DOMAIN = 1;
    private static final int FAMILY = 2;
    private static final int MEMBER = 3;
    //===============================================================
    //===============================================================
    public AttributeTree(List<HdbAttribute> attributeList)  {
        super();

        //  Create the nodes.
        rootNode = new DefaultMutableTreeNode("HDB++");
        createCollectionClassNodes(attributeList);

        //	Create the tree that allows one selection at a time.
        getSelectionModel().setSelectionMode
                (TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);

        //	Create Tree and Tree model
        DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
        setModel(treeModel);

        // Enable tool tips.
        ToolTipManager.sharedInstance().registerComponent(this);

        //  Set the icon for leaf nodes.
        TangoRenderer renderer = new TangoRenderer();
        setCellRenderer(renderer);

        setSelectionPath(null);
    }
    //===============================================================
    //===============================================================
    private void createCollectionClassNodes(List<HdbAttribute> attributeList) {
        List<AttCollection> csCollections = getAttCollection(attributeList, CS);
        for (AttCollection cs : csCollections) {
            DefaultMutableTreeNode csNode = new DefaultMutableTreeNode(cs);
            rootNode.add(csNode);
            List<AttCollection> domainCollections = getAttCollection(cs, DOMAIN);
            for (AttCollection domain : domainCollections) {
                DefaultMutableTreeNode domainNode = new DefaultMutableTreeNode(domain);
                csNode.add(domainNode);
                List<AttCollection> familyCollections = getAttCollection(domain, FAMILY);
                for (AttCollection family : familyCollections) {
                    DefaultMutableTreeNode familyNode = new DefaultMutableTreeNode(family);
                    domainNode.add(familyNode);
                    List<AttCollection> memberCollections = getAttCollection(family, MEMBER);
                    for (AttCollection member : memberCollections) {
                        DefaultMutableTreeNode memberNode = new DefaultMutableTreeNode(member);
                        familyNode.add(memberNode);
                        for (HdbAttribute attribute : member) {
                            DefaultMutableTreeNode attributeNode = new DefaultMutableTreeNode(attribute);
                            memberNode.add(attributeNode);
                        }
                    }
                }
            }
        }
    }
    //======================================================
    //======================================================
    private List<AttCollection> getAttCollection(List<HdbAttribute> attributeList, int item) {
        List<AttCollection> collectionList = new ArrayList<>();
        AttCollection collection = new AttCollection("",-1);
        String name;
        String str = "";
        for (HdbAttribute attribute : attributeList) {
            switch (item) {
                case CS:
                    name = attribute.getCsName();
                    break;
                case DOMAIN:
                    name = attribute.getDomainField();
                    break;
               case FAMILY:
                    name = attribute.getFamilyFiled();
                    break;
               case MEMBER:
                    name = attribute.getMemberField();
                    break;
                default:
                    System.err.println(item + " item not implemented");
                    return collectionList;
            }
            if (!name.equals(str)){
                collection = new AttCollection(name, item);
                collectionList.add(collection);
            }
            collection.add(attribute);
            str = name;
        }
        return collectionList;
    }
    //======================================================
    //======================================================
    @SuppressWarnings("unused")
    List<HdbAttribute> getSelectedAttributes() {
        List<HdbAttribute> attributeList = new ArrayList<>();
        // Get paths of all selected nodes
        TreePath[] treePaths = getSelectionPaths();
        if (treePaths!=null) {
            for (TreePath treePath : treePaths) {
                DefaultMutableTreeNode node =
                        (DefaultMutableTreeNode) treePath.getPathComponent(treePath.getPathCount() - 1);
                Object userObject = node.getUserObject();
                if (userObject instanceof HdbAttribute)
                    attributeList.add((HdbAttribute) userObject);
                else {
                    AttCollection collection = (AttCollection) userObject;
                    if (collection.item>=FAMILY)
                        attributeList.addAll(collection);
                }

            }
        }
        return attributeList;
    }
    //======================================================
    //======================================================
    List<String> getSelectedAttributeNames() {
        List<String> attributeList = new ArrayList<>();
        // Get paths of all selected nodes
        TreePath[] treePaths = getSelectionPaths();
        if (treePaths!=null) {
            for (TreePath treePath : treePaths) {
                DefaultMutableTreeNode node =
                        (DefaultMutableTreeNode) treePath.getPathComponent(treePath.getPathCount() - 1);
                Object userObject = node.getUserObject();
                if (userObject instanceof HdbAttribute)
                    attributeList.add(((HdbAttribute) userObject).getFullName());
                else {
                    AttCollection collection = (AttCollection) userObject;
                    if (collection.item>=FAMILY) {
                        for (HdbAttribute attribute : collection)
                            attributeList.add(attribute.getFullName());
                    }
                }

            }
        }
        return attributeList;
    }
    //======================================================
    //======================================================



    //======================================================
    //======================================================
    private class AttCollection extends ArrayList<HdbAttribute> {
        private String name;
        private int item;
        private AttCollection(String name, int item) {
            this.name = name;
            this.item = item;
        }
        @Override
        public String toString() {
            return name;
        }
    }
    //===============================================================
    //===============================================================




    //===============================================================
    /**
     * Renderer Class
     */
    //===============================================================
    private class TangoRenderer extends DefaultTreeCellRenderer {
        private Font rootNodeFont = new Font("Dialog", Font.BOLD, 18);
        private Font csFont = new Font("Dialog", Font.BOLD, 12);
        private Font attributeFont = new Font("Dialog", Font.PLAIN, 12);
        //===============================================================
        public Component getTreeCellRendererComponent(
                JTree tree,
                Object obj,
                boolean sel,
                boolean expanded,
                boolean leaf,
                int row,
                boolean hasFocus) {

            super.getTreeCellRendererComponent(
                    tree, obj, sel,
                    expanded, leaf, row,
                    hasFocus);

            setBackgroundNonSelectionColor(Color.white);
            setForeground(Color.black);
                 setBackgroundSelectionColor(Color.lightGray);
            setToolTipText(null);
            if (row == 0) {
                //	ROOT
                setFont(rootNodeFont);
                //setIcon(tangoIcon);
            } else {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) obj;
                if (node.getUserObject() instanceof AttCollection) {
                    setFont(csFont);
                    AttCollection collection = (AttCollection) node.getUserObject();
                    setToolTipText(collection.size() + " attributes");
                    //setIcon(classIcon);
                }
                else
                if (node.getUserObject() instanceof HdbAttribute) {
                    setFont(attributeFont);
                    HdbAttribute attribute = (HdbAttribute) node.getUserObject();
                    setToolTipText(attribute.toToolTip());
                    //setIcon(classIcon);
                }
            }
            return this;
        }
        //===============================================================
    }//	End of Renderer Class
    //==============================================================================
    //==============================================================================
}
