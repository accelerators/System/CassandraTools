//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  java source code for main swing class.
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,......,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//-======================================================================

package cassandra_tools.gui;

import cassandra_tools.commons.*;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

//=======================================================
/**
 *	JFrame Class to display info
 *
 * @author  Pascal Verdier
 */
//=======================================================
public class CassandraToolsFrame extends JFrame {
    private JFileChooser chooser = null;
    private String tangoHost;
    private List<String> srcAttributeList;
	//=======================================================
    /**
	 *	Creates new form CassandraAttributeFrame
	 */
	//=======================================================
    public CassandraToolsFrame(String fileName) throws DevFailed {
        initComponents();
        splitPane.setDividerLocation(0.5);
        tangoHost = Utils.getTangoHost();
        tangoHostTextField.setText(tangoHost);
        if (fileName==null) {
            //  Get attribute list from att_conf table to be selected
            selectFromDatabase();
        }
        else
            displayFileContent(fileName);
        setTitle(Utils.getInstance().getApplicationRelease());
        pack();
        ATKGraphicsUtils.centerFrameOnScreen(this);
	}
	//=======================================================
	//=======================================================
    private void selectFromDatabase() throws DevFailed {
        CassandraConnection.getInstance().updateArchiveAttributes();
        List<HdbAttribute> attributeList = CassandraConnection.getInstance().getHdbAttributeList();
        AttributeTreeDialog dialog = new AttributeTreeDialog(this, attributeList);
        srcAttributeList = dialog.showDialog();
        if (srcAttributeList.size()>0) {
            srcTextArea.setText(Utils.lines2text(srcAttributeList));
            convertButtonActionPerformed(null);
        }
    }
	//=======================================================
	//=======================================================
    private void displayFileContent(String fileName) throws DevFailed {
        srcAttributeList = getAttributeList(fileName);
        srcTextArea.setText(Utils.lines2text(srcAttributeList));
        convertButtonActionPerformed(null);
    }
	//=======================================================
	//=======================================================
    private List<String> getAttributeList(String fileName) throws DevFailed {
        List<String> lines = Utils.readFileLines(fileName);
        List<String> attributeList = new ArrayList<>();
        for (String line :lines) {
            if (line.startsWith("tango://")) {
                int idx = line.indexOf(";");
                if (idx>0)
                    line = line.substring(0, idx);
                attributeList.add(line);
            }
        }
        return attributeList;
    }
	//=======================================================
	//=======================================================
    private void checkIfAttributesAlreadyExist(List<String> attributeNames) throws DevFailed {
        CassandraConnection.getInstance().updateArchiveAttributes();
        StringBuilder sb = new StringBuilder();
        for (String attributeName : attributeNames) {
            if (CassandraConnection.getInstance().getHdbAttribute(attributeName)!=null) {
                sb.append(attributeName).append("  already exists\n");
            }
        }
        if (sb.length()>0)
            Except.throw_exception("AlreadyExist", sb.toString().trim());
    }
	//=======================================================
	//=======================================================

	//=======================================================
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
	//=======================================================
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel jPanel1 = new javax.swing.JPanel();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        tangoHostTextField = new javax.swing.JTextField();
        javax.swing.JLabel jLabel4 = new javax.swing.JLabel();
        javax.swing.JLabel jLabel5 = new javax.swing.JLabel();
        domainTextField = new javax.swing.JTextField();
        javax.swing.JLabel jLabel6 = new javax.swing.JLabel();
        javax.swing.JButton convertButton = new javax.swing.JButton();
        javax.swing.JLabel jLabel7 = new javax.swing.JLabel();
        javax.swing.JButton createButton = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        javax.swing.JPanel jPanel2 = new javax.swing.JPanel();
        javax.swing.JLabel jLabel2 = new javax.swing.JLabel();
        javax.swing.JScrollPane srcScrollPane = new javax.swing.JScrollPane();
        srcTextArea = new javax.swing.JTextArea();
        javax.swing.JPanel jPanel3 = new javax.swing.JPanel();
        javax.swing.JLabel jLabel3 = new javax.swing.JLabel();
        javax.swing.JScrollPane newScrollPane = new javax.swing.JScrollPane();
        newTextArea = new javax.swing.JTextArea();
        javax.swing.JMenuBar menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem databaseItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem openItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem releaseItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem jMenuItem1 = new javax.swing.JMenuItem();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        jLabel1.setText("TANGO_HOST: ");
        jPanel1.add(jLabel1);

        tangoHostTextField.setColumns(20);
        tangoHostTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertButtonActionPerformed(evt);
            }
        });
        jPanel1.add(tangoHostTextField);

        jLabel4.setText("      ");
        jPanel1.add(jLabel4);

        jLabel5.setText("Domain: ");
        jPanel1.add(jLabel5);

        domainTextField.setColumns(8);
        domainTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertButtonActionPerformed(evt);
            }
        });
        jPanel1.add(domainTextField);

        jLabel6.setText("      ");
        jPanel1.add(jLabel6);

        convertButton.setText(" Convert ");
        convertButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        convertButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertButtonActionPerformed(evt);
            }
        });
        jPanel1.add(convertButton);

        jLabel7.setText("                  ");
        jPanel1.add(jLabel7);

        createButton.setText("Create Attr.");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });
        jPanel1.add(createButton);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        splitPane.setPreferredSize(new java.awt.Dimension(1100, 500));

        jPanel2.setLayout(new java.awt.BorderLayout());

        jLabel2.setText("SRC attribute List");
        jPanel2.add(jLabel2, java.awt.BorderLayout.NORTH);

        srcTextArea.setEditable(false);
        srcTextArea.setColumns(20);
        srcTextArea.setRows(5);
        srcScrollPane.setViewportView(srcTextArea);

        jPanel2.add(srcScrollPane, java.awt.BorderLayout.CENTER);

        splitPane.setLeftComponent(jPanel2);

        jPanel3.setLayout(new java.awt.BorderLayout());

        jLabel3.setText("NEW attribute List");
        jPanel3.add(jLabel3, java.awt.BorderLayout.NORTH);

        newTextArea.setColumns(20);
        newTextArea.setRows(5);
        newScrollPane.setViewportView(newTextArea);

        jPanel3.add(newScrollPane, java.awt.BorderLayout.CENTER);

        splitPane.setRightComponent(jPanel3);

        getContentPane().add(splitPane, java.awt.BorderLayout.CENTER);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");

        databaseItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        databaseItem.setMnemonic('D');
        databaseItem.setText("Open from HDB");
        databaseItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                databaseItemActionPerformed(evt);
            }
        });
        fileMenu.add(databaseItem);

        openItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openItem.setMnemonic('O');
        openItem.setText("Open File");
        openItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openItemActionPerformed(evt);
            }
        });
        fileMenu.add(openItem);

        exitItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitItem.setMnemonic('E');
        exitItem.setText("Exit");
        exitItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitItem);

        menuBar.add(fileMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("help");

        releaseItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        releaseItem.setMnemonic('R');
        releaseItem.setText("Release Notes");
        releaseItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                releaseItemActionPerformed(evt);
            }
        });
        helpMenu.add(releaseItem);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setMnemonic('A');
        jMenuItem1.setText("About");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItem1);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void openItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openItemActionPerformed
		if (chooser==null) {
			chooser = new JFileChooser(new File("").getAbsolutePath());
		}
		if (chooser.showOpenDialog(this)==JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			if (file!=null) {
				if (file.isFile()) {
				    try {
                        String filename = file.getAbsolutePath();
                        displayFileContent(filename);
                    }
				    catch (DevFailed e) {
				        ErrorPane.showErrorMessage(this, null, e);
                    }
				}
			}
		}
    }//GEN-LAST:event_openItemActionPerformed
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void exitItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitItemActionPerformed
        doClose();
    }//GEN-LAST:event_exitItemActionPerformed
	//=======================================================
	//=======================================================
    @SuppressWarnings("UnusedParameters")
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        doClose();
    }//GEN-LAST:event_exitForm
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        String  message = Utils.getInstance().getApplicationRelease() +
                "\nThis application is able to add attribute(s) to HDB++ with another name (new CS or domain)\n" +
                "\nPascal Verdier - Accelerator Control Unit";
        JOptionPane.showMessageDialog(this, message, "Help Window", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem1ActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void convertButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_convertButtonActionPerformed
        tangoHost = tangoHostTextField.getText().trim();
        List<String> attributeList = Utils.replaceTangoHost(srcAttributeList, tangoHost);
        String domain = domainTextField.getText().trim();
        if (!domain.isEmpty()) {
            attributeList = Utils.replaceDomain(attributeList, domain);
        }
        newTextArea.setText(Utils.lines2text(attributeList));
    }//GEN-LAST:event_convertButtonActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
         // TODO
         //  Get list of new attributes and check size
         List<String> newAttributeList = Utils.text2lines(newTextArea.getText());
         if (newAttributeList.size()!=srcAttributeList.size()) {
             ErrorPane.showErrorMessage(this, null,
                     new Exception("SRC list  and  NEW list do not have same size !"));
             return;
         }
         try {
             checkIfAttributesAlreadyExist(newAttributeList);
         } catch (DevFailed e) {
             ErrorPane.showErrorMessage(this, null, e);
             return;
         }

         if (JOptionPane.showConfirmDialog(this,
                "Create attributes in HDB++ ?", "confirm",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

             // OK ! Start a splash screen and add attributes to HDB
             SplashUtils.getInstance().startSplash();
             double ratio = 100.0/srcAttributeList.size();
             if (ratio<1) ratio = 1;
             int idx = 0;
             StringBuilder sbError = new StringBuilder();
             for (int i=0 ; i< srcAttributeList.size() ; i++) {
                 SplashUtils.getInstance().setSplashProgress((idx+=ratio), "Adding " + newAttributeList.get(i));
                 try {
                     CassandraConnection.getInstance().addNewAttribute(
                             srcAttributeList.get(i), newAttributeList.get(i));
                 }
                 catch (Error | Exception e) {
                     if (e instanceof DevFailed)
                         sbError.append(((DevFailed)e).errors[0].desc).append('\n');
                     else {
                         SplashUtils.getInstance().stopSplash();
                         if (e instanceof Error)
                             ErrorPane.showErrorMessage(this, null, new Exception(e.toString()));
                         else
                             ErrorPane.showErrorMessage(this, null, (Exception) e);
                         e.printStackTrace();
                         return;
                     }
                 }
             }
             SplashUtils.getInstance().stopSplash();

             // Check errors
             if (sbError.length()>0) {
                 String error = Utils.checkReturnNumber(sbError.toString());
                 ErrorPane.showErrorMessage(this, null, new Exception(error));

             }
        }
    }//GEN-LAST:event_createButtonActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void releaseItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_releaseItemActionPerformed
        new PopupHtml(this).show(ReleaseNotes.htmlString, 450, 450);
    }//GEN-LAST:event_releaseItemActionPerformed
    //=======================================================
    //=======================================================
    @SuppressWarnings("UnusedParameters")
    private void databaseItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_databaseItemActionPerformed
        try {
            selectFromDatabase();
        }
        catch (DevFailed e) {
            ErrorPane.showErrorMessage(this, null, e);
        }
    }//GEN-LAST:event_databaseItemActionPerformed
	//=======================================================
	//=======================================================
    private void doClose() {
        System.exit(0);
    }
	//=======================================================
	//=======================================================



	//=======================================================
    /**
     * @param args the command line arguments
     */
	//=======================================================
    public static void main(String[] args) {
        String fileName = null;
        if (args.length>0)
            fileName = args[0];
		try {
      		new CassandraToolsFrame(fileName).setVisible(true);
		}
		catch(DevFailed e) {
            ErrorPane.showErrorMessage(new Frame(), null, e);
			System.exit(0);
		}
    }
	//=======================================================
	//=======================================================





	//=======================================================
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField domainTextField;
    private javax.swing.JTextArea newTextArea;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JTextArea srcTextArea;
    private javax.swing.JTextField tangoHostTextField;
    // End of variables declaration//GEN-END:variables
	//=======================================================

}
